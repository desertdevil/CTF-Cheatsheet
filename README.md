# Binary Exploitation Tips
* Always run `checksec` on the binary 
* RELRO - Reallocation Read Only
* NX - Non Executable segment in memory
    ** a page cannot excute and be written to at the same time
* ASLR - Address Stack Layout Randomization
    ** - Some areas of the stack memory are randomized
* PIE - Position Independent Executable
    ** - All sections of the binary are randomized
* stack pivot: create a fake stack frame to ease ROP chaining
* pointer dereferencing: use of registers to resolve runtime offset

# FAQ
"How do I get started with CTFS, I am a complete noob"
Start learning how to use UNIX commands and the fundamentals of Linux
[Bandit OverTheWire](http://overthewire.org/wargames/bandit/)
"Okay, I did Bandit how to I start CTFs for real now?"
There are many areas of CTF's including but not limited to...
* Binary Exploitation and Pwning
* Web security
* Reverse Engineering
* Cryptography
* Forensics  
Practice CTF challenges in these areas to get a good grasp of the challenges to expect.

# Web Security Challenges
[Natas OverTheWire](http://overthewire.org/wargames/bandit/)
[OWASP Juice Box](http://overthewire.org/wargames/bandit/)

# Binary Exploitation and Pwning
[Protostar Exploit Exercises](https://exploit-exercises.com/protostar/)
[Pwnable.kr](http://pwnable.kr)

#Web Security
[Natas OverTheWire](http://overthewire.org/wargames/natas/)
[OWASP Juice Shop](https://www.owasp.org/index.php/OWASP_Juice_Shop_Project)

# Sources
http://blog.siphos.be/2011/07/high-level-explanation-on-some-binary-executable-security/



